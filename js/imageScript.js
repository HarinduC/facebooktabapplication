$(document).ready(function(){

    $("#delete-dialog").dialog({
        autoOpen: false,
        height: 200,
        width: 500,
        modal: true,
        buttons: {
            "Yes": function() {
                
                var imageSrc = $(this).find("#image_src").val();
                
                deleteImage(imageSrc);

                $(this).dialog("close");
            },
            "No": function() {
                $(this).dialog("close");
            }
        }
    });
    
    $(document).on("click","#del-image",function(e) {
        var imgSrc = $(this).attr("img_id");
        $("#delete-dialog").find("#image_src").val(imgSrc);
        $("#delete-dialog").dialog("open");
    
    });

    
    

    

    
    function deleteImage(imageSrc){
    
	   $.ajax({
		  url:"delete.image.php",
		  type:"POST",
		  data:{imageSrc:imageSrc},
		  success: function(){
              location.reload();
              console.log("delete image");
		
		}
	}); 
    
}


}
);

function updatepicture(pic){
        document.getElementById("image").setAttribute("src",pic);
}

function fileTypeError(){
    parent.document.getElementById("message").innerHTML = "Invalid file format ! Please select one of the following formats : jpeg,png or gif";
    }
function unknownError(){
	   parent.document.getElementById("message").innerHTML = "Unknown Error";

        
    }

 function noFileError(){ 
   parent.document.getElementById("message").innerHTML = "No file is seledcted ! Please select a file";
     
    }

function invalidParameters(){
	   
        parent.document.getElementById("message").innerHTML = "Please select a proper file";
    }      

   

    function fileSizeError(){
	  
        parent.document.getElementById("message").innerHTML = "Your File size exceeds the maximum limit";
    }

    

