<?php

try{

	if(!isset($_FILES['file']['error'])|| is_array($_FILES['file']['error'])){

		?>
		<script type="text/javascript">
		window.parent.invalidParameters();
		</script>
		<?php
		throw new RuntimeException('Invalid Parameters');
		

	}

// Check $_FILES['file']['error'] value.
    switch ($_FILES['file']['error']) {
        case UPLOAD_ERR_OK:
			break;
        case UPLOAD_ERR_NO_FILE:
            ?>
			<script type="text/javascript">
			window.parent.noFileError();
			</script>
			<?php
			throw new RuntimeException('No file sent.');
			
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
			 ?>
			<script type="text/javascript">
			window.parent.fileSizeError(); 
			</script>
			<?php
            throw new RuntimeException('Exceeded filesize limit.');
        default:
			?>
			<script type="text/javascript">
			window.parent.unknownError(); 
			</script>
			<?php
            throw new RuntimeException('Unknown errors.');
    }
	
	// You should also check filesize here. 
    if ($_FILES['file']['size'] > 8388608) {
			?>
			<script type="text/javascript">
			window.parent.fileSizeError(); 
			</script>
			<?php
		throw new RuntimeException('Exceeded filesize limit.');
    }
	
	$finfo = new finfo(FILEINFO_MIME_TYPE);
    if (false === $ext = array_search(
        $finfo->file($_FILES['file']['tmp_name']),
        array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
        ),
        true
    )) {
			?>
			<script type="text/javascript">
			window.parent.fileTypeError();
			</script>
			<?php
        throw new RuntimeException('Invalid file format.');
    }
		
	if(move_uploaded_file($_FILES['file']['tmp_name'],"images/".$_FILES['file']['name'])){
		//file uploaded
		?>
		<script type="text/javascript">
            parent.document.getElementById("message").innerHTML = "Uploaded successfully . Following is the preview ";
			parent.document.getElementById("file").value ="";
			window.parent.updatepicture("<?php echo 'images/'.$_FILES['file']['name'];?>");
        </script> 
		<?php
		}
		else{
			//upload failed
			?>
			<script type="text/javascript">
				parent.document.getElementById("message").innerHTML = "There was an error uploading the image.";
			</script>
			
			<?php
			
		}
	}
	
	catch(RuntimeException $e){
	
		 echo $e->getMessage();
		 
	}
	
?>
<script src="js/imageScript.js"></script>


