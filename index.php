<?php session_start();

?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/jquery-ui.min.css" rel="stylesheet">
        
    <title>PHP Javascript Image Uploader</title>
    <script src="js/jquery-1.11.1.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/imageScript.js"></script>
    <script src="js/jquery-ui.min.js"></script>
        
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">PHP Javascript Image Uploader</h1>
    
            <br/>
                <form  class="form-inline" role="form" id="form" method="post" action="upload.php" enctype="multipart/form-data" target="iframe" >
                    <div class="form-group">
                        <label for="file">Select an Image:</label>
                        <input type="file" class="form-control" id="file" name="file" />
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Upload Image"/>    
                    </div>
                    
            
                </form>
              <div class="container" align="center">
                <p id="message"></p>
                <div id="alert"></div>
                <img class="img-rounded" align="middle" style="min-height:120;min-width:200;max-height:200px;max-width:200px;"  id="image" src="css/upload.png"/>                   </br></br>
		      </div>
		<div class="container" align="center">
		<?php
		
		$dir = "images/";
		$arr = scandir($dir);
		$newArray = $arr;
		
		$_SESSION['newArray'] = $newArray;
		
		$arrlength = count($newArray);
		
		echo "<table class='table table-bordered'>";
		echo "<tr class='info'><th>Image Preview</th><th>Action</th></tr>";
		for($i=2;$i<$arrlength;$i++){
			
			
			echo "<tr>" ;
			echo "<td><img name='img$i' src='images/$newArray[$i]' style='max-height:100px;max-width:200px;min-height:120;min-width:200;' /></td>";
			echo "<td><button id='del-image' class='btn btn-warning' img_id='$newArray[$i]')>Delete Image</button></td>";
			echo "</tr>";
			}
		echo "</table>" ;

		?>
		</div>
		
        <div id="delete-dialog" title="Confirm Delete">
            <input type="hidden" id="image_src" />
            <p id="delete-dialog-text">
                Do you really want to remove this company?
            </p>
        </div>
    
        <iframe style="display:none;" name="iframe" width="900px" height="500px" >
        </iframe>
    </div>
    </body>
    
</html>